import React, {useEffect} from 'react';
import {useSelector, useDispatch} from "react-redux";
import Burger from "../../components/Burger/Burger";
import BuildControls from "../../components/Burger/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import OrderSummary from "../../components/Burger/OrderSummary/OrderSummary";
import {addIngredient, initIngredients, removeIngredient, setPurchasing} from "../../store/actions/burgerActions";
import {initOrder} from "../../store/actions/ordersActions";

const BurgerBuilder = props => {

  const ingredients = useSelector(state => state.burger.ingredients);
  const totalPrice = useSelector(state => state.burger.totalPrice);
  const purchasing = useSelector(state => state.burger.purchasing);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(initIngredients());
  }, []);

  const addIngredientHandler = ingName => {
    dispatch(addIngredient(ingName));
  };

  const removeIngredientHandler = ingName => {
    dispatch(removeIngredient(ingName));
  };

  const isPurchasable = () => {
    const sum = Object.values(ingredients)
      .reduce((sum, el) => sum + el, 0);

    return sum > 0;
  };

  const purchaseHandler = () => {
    dispatch(setPurchasing(true));
  };

  const purchaseCancelHandler = () => {
    dispatch(setPurchasing(false));
  };

  const purchaseContinueHandler = () => {
    dispatch(setPurchasing(false));
    dispatch(initOrder());
    props.history.push('/checkout');
  };

  const disabledInfo = {...ingredients};

  Object.keys(ingredients).forEach(key => {
    disabledInfo[key] = disabledInfo[key] <= 0;
  });

  return (
    <>
      <Modal show={purchasing} closed={purchaseCancelHandler}>
        <OrderSummary
          ingredients={ingredients}
          price={totalPrice}
          purchaseCancelled={purchaseCancelHandler}
          purchaseContinued={purchaseContinueHandler}
        />
      </Modal>
      <Burger ingredients={ingredients} />
      <BuildControls
        ingredients={ingredients}
        price={totalPrice}
        ingredientAdded={addIngredientHandler}
        ingredientRemoved={removeIngredientHandler}
        disabledInfo={disabledInfo}
        purchasable={isPurchasable()}
        ordered={purchaseHandler}
      />
    </>
  );
};

export default BurgerBuilder;
