import React, {useEffect} from 'react';
import axiosOrders from "../../axios-orders";
import OrderItem from "../../components/Order/OrderItem/OrderItem";
import Spinner from "../../components/UI/Spinner/Spinner";
import withErrorHandler from "../../hoc/withErrorHandler";
import ErrorBoundary from "../../components/ErrorBoundary/ErrorBoundary";
import {useDispatch, useSelector} from "react-redux";
import {fetchOrders} from "../../store/actions/ordersActions";


const Orders = () => {
  const orders = useSelector(state => state.orders.orders);
  const loading = useSelector(state => state.orders.loading);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchOrders());
  },[]);

  let ordersOutput = orders.map(order => (
    <ErrorBoundary key={order.id}>
      <OrderItem
        ingredients={order.ingredients}
        price={order.price}
      />
    </ErrorBoundary>
  ));

  if (loading) {
    ordersOutput = <Spinner/>;
  }

  return ordersOutput;
};

export default withErrorHandler(Orders, axiosOrders);