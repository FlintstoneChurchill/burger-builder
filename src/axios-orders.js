import axios from 'axios';

const axiosOrders = axios.create({
  baseURL: 'https://burger-builder-19265.firebaseio.com/'
});

export default axiosOrders;