import React from 'react';
import './OrderItem.css';

const OrderItem = props => {
  const ingredients = Object.keys(props.ingredients).map(igName => {
    return {
      name: igName,
      amount: props.ingredients[igName]
    }
  });

  const ingredientOutput = ingredients.map(ig => (
    <span key={ig.name}>{ig.name} ({ig.amount})</span>
  ));

  if (Math.random() > 0.7) throw new Error('Well, this happened');

  return (
    <div className="OrderItem">
      <p>Ingredients: {ingredientOutput}</p>
      <p>Price: <strong>{props.price} KGS</strong></p>
    </div>
  );
};

export default OrderItem;