import {INGREDIENT_PRICES} from "../../constants";
import {ADD_INGREDIENT, INIT_INGREDIENTS, REMOVE_INGREDIENT, SET_PURCHASING} from "../actionTypes";

const INITIAL_INGREDIENTS = {
  salad: 0,
  bacon: 0,
  cheese: 0,
  meat: 0
};
const INITIAL_PRICE = 20;

const initialState = {
  ingredients: {...INITIAL_INGREDIENTS},
  totalPrice: INITIAL_PRICE,
  purchasing: false
};

const burgerReducer = (state = initialState, action) => {
  switch(action.type) {
    case ADD_INGREDIENT:
      return {
        ...state,
        ingredients: {
          ...state.ingredients,
          [action.ingName]: state.ingredients[action.ingName] + 1
        },
        totalPrice: state.totalPrice + INGREDIENT_PRICES[action.ingName]
      };
    case REMOVE_INGREDIENT:
      return {
        ...state,
        ingredients: {
          ...state.ingredients,
          [action.ingName]: state.ingredients[action.ingName] - 1
        },
        totalPrice: state.totalPrice - INGREDIENT_PRICES[action.ingName]
      };
    case SET_PURCHASING:
      return {
        ...state,
        purchasing: action.purchasing
      }
    case INIT_INGREDIENTS:
      return {
        ...state,
        ingredients: {...INITIAL_INGREDIENTS},
        totalPrice: INITIAL_PRICE
      }
    default:
      return state;
  }
};

export default burgerReducer;