import {
  FETCH_ORDERS_FAILURE,
  FETCH_ORDERS_REQUEST,
  FETCH_ORDERS_SUCCESS,
  INIT_ORDER,
  ORDER_FAILURE,
  ORDER_REQUEST,
  ORDER_SUCCESS
} from "../actionTypes";

const initialState = {
  loading: false,
  error: null,
  fetchError: null,
  ordered: false,
  orders: []
};

const ordersReducer = (state = initialState, action) => {
  switch (action.type) {
    case ORDER_REQUEST:
      return {...state, loading: true};
    case ORDER_SUCCESS:
      return {...state, loading: false, ordered: true};
    case ORDER_FAILURE:
      return {...state, loading: false, error: action.error};
    case FETCH_ORDERS_REQUEST:
      return {...state, loading: true};
    case FETCH_ORDERS_SUCCESS:
      return {...state, loading: false, orders: action.orders};
    case FETCH_ORDERS_FAILURE:
      return {...state, loading: false, fetchError: action.error};
    case INIT_ORDER:
      return {...state, ordered: false};
    default:
      return state;
  }
};

export default ordersReducer;