import {
  FETCH_ORDERS_FAILURE,
  FETCH_ORDERS_REQUEST,
  FETCH_ORDERS_SUCCESS,
  INIT_ORDER,
  ORDER_FAILURE,
  ORDER_REQUEST,
  ORDER_SUCCESS
} from "../actionTypes";
import axios from "../../axios-orders";

const orderRequest = () => {
  return {type: ORDER_REQUEST};
};
const orderSuccess = () => {
  return {type: ORDER_SUCCESS};
};
const orderFailure = error => {
  return {type: ORDER_FAILURE, error};
};

export const createOrder = order => {
  return async dispatch => {
    dispatch(orderRequest());
    try {
      const response = await axios.post("/orders.json", order);
      dispatch(orderSuccess(response.data));
    } catch(e) {
      dispatch(orderFailure(e));
    }
  };
};

const fetchOrdersRequest = () => {
  return {type: FETCH_ORDERS_REQUEST};
};
const fetchOrdersSuccess = orders => {
  return {type: FETCH_ORDERS_SUCCESS, orders};
};
const fetchOrdersFailure = error => {
  return {type: FETCH_ORDERS_FAILURE, error};
};

export const fetchOrders = () => {
  return async dispatch => {
    dispatch(fetchOrdersRequest());
    try {
      const response = await axios.get('/orders.json');
      const fetchedOrders = Object.keys(response.data).map(id => {
        return {...response.data[id], id};
      });
      dispatch(fetchOrdersSuccess(fetchedOrders));
    } catch(e) {
      dispatch(fetchOrdersFailure(e));
    }

  };
};

export const initOrder = () => {
  return {type: INIT_ORDER};
};