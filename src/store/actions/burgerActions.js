import {
  ADD_INGREDIENT,
  INIT_INGREDIENTS,
  REMOVE_INGREDIENT,
  SET_PURCHASING
} from "../actionTypes";

export const addIngredient = ingName => {
  return {type: ADD_INGREDIENT, ingName};
};
export const removeIngredient = ingName => {
  return {type: REMOVE_INGREDIENT, ingName};
};
export const setPurchasing = purchasing => {
  return {type: SET_PURCHASING, purchasing: purchasing}
};

export const initIngredients = () => {
  return {type: INIT_INGREDIENTS};
};

